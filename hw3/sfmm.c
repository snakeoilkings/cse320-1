#include "sfmm.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>

#if defined LIFO && defined ADDRESS
#error "Cannot define both LIFO and ADDRESS placement strategies."
#endif

#if defined NEXT && defined FIRST
#error "Cannot define both NEXT and FIRST placement strategies."
#endif


int* heap_ptr=0; /* heap start address */
int* heap_end=0; /* heap end address*/
int* root;		 /* start of linked list*/
int* tail;		 /* not sure how this is working yet*/
int* sf_cursor;	 /* where we are at*/
unsigned int heap_size=0;
int LIFO_c = 1;
int ADDRESS_c = 0;
int FIRST_c = 1;
int NEXT_c = 0;


/* Initializes memory by making an aligned header that signifies the end of 
the space on the heap */
void sf_mem_init(void){
	int64_t current_add = (int64_t)sbrk(0);
	sbrk(4096);
	heap_size = 4096;
	heap_ptr = (int*)current_add;
	heap_end = heap_ptr + 1022;
	memset(heap_ptr, '\0', 8);
	/*makes sure if you free the first block it doesn't think it needs to coalesce*/
	*(heap_ptr+1) |= 1 << 0;
	/*sets heap to = 0 size and 0 free space*/
	int* ptr = heap_ptr+2;
	*ptr = 4064;
	int* size = heap_ptr+3;
	*size = (4080/16) << 3;
	/* This is root, so we'll make it point to itself for prev, and next */
	int* prev = heap_ptr+6;
	*prev = (int64_t)ptr;
	int* next = heap_ptr+4; 
	*next = (int64_t)ptr;
	/*creates footer for first block */
	int* footer = heap_ptr+1020;
	*footer = 4064;
	*(footer+1) = (4080/16) << 3;
	/*makes final block empty (padding)*/
	int* end_block = heap_ptr+1022;
	memset(end_block, '\0', 8);
	root = ptr;
	tail = ptr;
	sf_cursor = root;
}

void* sf_malloc(size_t size) {
	if (size >= 4294967296 || size==0){
		printf("ERROR: %s\n", strerror(ENOMEM));
		return NULL;
	}
	#ifdef NEXT
	NEXT_c = 1;
	FIRST_c = 0;
	#endif
	#ifdef FIRST
	FIRST_c = 1;
	NEXT_c = 0;
	#endif
	#ifdef LIFO
	LIFO_c = 1;
	ADDRESS_c = 0;
	#endif
	#ifdef ADDRESS
	ADDRESS_c = 1;
	LIFO_c = 0;
	#endif
    /*initialize the heap*/
    if (heap_ptr == 0)
        sf_mem_init();
    int* cursor = root;
	
	if (NEXT_c == 1)
		cursor = sf_cursor;
	if (FIRST_c == 1)
		cursor = cursor;

	
    /*calculate padding*/
    int padding = size % 16;
    if (padding != 0)
        padding = 16-padding;
	
    /*set up new vars*/
    int not_found = 1;
    unsigned int available_size, block_size;
    int* footer;
	int* start_cursor = sf_cursor;
	int cursor_flag=0;
	int is_full;
	
    while (not_found) {
        available_size = *cursor;
        cursor++;
        block_size = (*cursor >> 1)*4;
		/*check for full list*/
		is_full = *(cursor) & 1;
		if (available_size >= size && is_full==0){
			int remaining_split = available_size - (size+padding);
			/*checks to see if we should split, or use the rest for padding*/
			if (remaining_split < 32) {
				padding+=remaining_split;
				if (remaining_split != 0 && remaining_split !=16) {
					/*we have a word boundary problem if we are here*/
					printf("ERROR: %s\n", strerror(ENOMEM));
					return NULL;
				} 
				remaining_split=0;
			}
			cursor--;
			/*change header*/
			*cursor = size;
			cursor++;
			*cursor = ((size+padding+16)/16) << 3;
			*cursor |= 1 << 0;
			/*change footer*/
			footer = cursor + (((size+padding)/4)+1);
			*footer = padding;
			footer++;
			*footer = ((size+padding+16)/16) << 3;
			*footer |= 1 << 0;
			if (remaining_split != 0) {
				/*create new header*/
				int remaining_size = block_size - (size+padding+16);
				footer++;
				int* new_start = footer;
				sf_cursor = new_start;
				*footer=remaining_size-16;
				footer++;
				*footer = (remaining_size/16) << 3;
				/*create new next*/
				int* next = (cursor+1);
				int64_t next2 = *next;
				*next = next2;
				footer++;
				*footer = (int64_t)*next;
				/*create new prev*/
				footer = footer+2;
				int* prev = cursor+3;
				int64_t prev2 = *prev;
				*footer= prev2;
				/*change old footer*/
				footer = (cursor-2) + (block_size/4);
				*footer = (remaining_size/16) << 3;
				footer--;
				*footer = remaining_size-16;
				/*change previous next to jump over used space*/
				prev = (int*)((int64_t)*(cursor+3));
				prev = prev + 2;
				*prev = (int64_t)(new_start);
				/*change next's previous to jump to new header*/
				next = cursor + 1;
				int64_t new_next = *next;
				next = (int*)new_next;
				next+=4;
				*next = (int64_t)(new_start);
				/*if cursor was the tail, now the new split block is*/
				if (ADDRESS_c == 1) {
					if ((cursor-1) == tail){
						if (tail == root){
							tail = new_start;
							root = new_start;
							*(new_start+2) = (int64_t)root;
							*(new_start+4) = (int64_t)root;
						}
						else
							tail = new_start;
					}
				}
				if (LIFO_c == 1) {
					if ((cursor-1) == tail){
						if (tail == root)
							tail = new_start;
						else
							tail = (int*)((int64_t)*(cursor+3));
					}
					root = new_start;
				}
			}
			else {
				/*change previous next to jump over used space*/
				int* prev = (int*)((int64_t)*(cursor+3));
				prev = prev + 2;
				*prev = (int64_t)*(cursor + 1);
				/*check to see if we're at tail*/
				if ((cursor-1)==tail){
					tail = prev-2;
					*prev = (int64_t)root;
				}
				/*change next previous to jump back and skip over used space*/
				int *new_prev = cursor + 1;
				int64_t new_prev2 = *new_prev;
				new_prev = (int*)new_prev2;
				new_prev+=4;
				new_prev2 = (int64_t)*(cursor+3);
				*new_prev = new_prev2;
				sf_cursor = (int*)((int64_t)*(cursor + 1));
			}
			not_found=0;
		}
		else {
			
			/*check to see if we've hit every element in linked list for NEXT*/
			if (cursor == start_cursor)
				cursor_flag++;
			if (((cursor-1) == tail && FIRST_c == 1) || cursor_flag==2 || is_full!=0) {
				/*we have reached end of heap, add more space*/
				sbrk(4096);
				heap_size+=4096;
				/*Check for coalescing*/
				int final_used = *(heap_end - 1) & 1;
				unsigned int final_size = (*(heap_end - 1) >> 1)*4;
				cursor = heap_end - (final_size/4) + 1;
				int* endof_block = cursor + (available_size/4) + 3;
				/*checks to see if a free block is attached to new free mem*/
				if (endof_block == heap_end && final_used==0) {
					/*change block header to include new space*/
					*(cursor-1) = *(cursor-1) + 4096;
					*(cursor) = ((*(cursor-1) + 16)/16) << 3;
					/*change block footer*/
					heap_end+=1024;
					footer = heap_end-2;
					*footer = *(cursor-1);
					*(footer+1) = *(cursor);
					/*retry block with new size*/
					cursor--;
					if (LIFO_c==1) {
						*(root+4) = (int64_t)cursor;
						*(cursor+2) = (int64_t)root;
						*(cursor+4) = (int64_t)tail;
						root=cursor;
						if (cursor==tail)
							tail = (int*)(int64_t)*(tail+4);
					}
				}
				else {
					/*create header*/
					*(heap_end) = 4080;
					*(heap_end+1) = (4096/16) << 3;
					/*create prev*/
					if ((*(root+1)&1) == 1) 
						root = heap_end;
					if ((*(tail+1)&1) == 1) 
						tail = heap_end;
					if(root == heap_end && tail == heap_end)
						sf_cursor = root;
					*(heap_end+4) = (int64_t)root;
					/*create next*/
					*(heap_end+2) = (int64_t)tail;
					/*create footer*/
					*(heap_end+1022) = 4080;
					*(heap_end+1023) = (4096/16) << 3;
					/*make new tail*/
					if (ADDRESS_c==1)
						tail = heap_end;
					if (LIFO_c==1) {
						if (root==tail)
							tail = heap_end;
						*(root+4) = (int64_t)heap_end;
						*(heap_end+2) = (int64_t)root;
						*(heap_end+4) = (int64_t)tail;
						root=heap_end;
					}
					/*make new heap_end*/
					heap_end+=1024;
					/*change next of tail */
					*(tail+2) = (int64_t)heap_end;
					/*try new spot*/
					cursor++;
					cursor = (int*)((int64_t)*(cursor));
					if(root == heap_end-1024 && tail == heap_end-1024)
						cursor = root;
				}
            }
			else {
				cursor++;
				cursor = (int*)((int64_t)*(cursor));
			}
        }
    }
    cursor++;
    return (void*)cursor;
}

void sf_free(void *ptr){
	#ifdef LIFO
	LIFO_c = 1;
	ADDRESS_c = 0;
	#endif
	#ifdef ADDRESS
	ADDRESS_c = 1;
	LIFO_c = 0;
	#endif
	if (ptr==NULL) {
		printf("ERROR: %s\n", strerror(ENOMEM));
		return;
	}
	/*
	int64_t address = *((int*)ptr) - 1;
	int* check_ptr = (int*)address;
	unsigned int check = *(check_ptr) >> 1;
	
	if (check%16 != 0) {
		printf("ERROR: Trying to access and invalid memory address\n.");
		return;
	}*/
		
	/*checks to see if space is already free*/
	int* target = ((int*)ptr)-2;
	unsigned int available_size = (*(target+1) >> 1)*4;
	/*checking boundary tags for coalescing*/
	int prev_free = *(target-1) & 1;
	int next_free = *(target+1+(available_size/4)) & 1;
	int* header;
	if (LIFO_c == 1) {
	/*CASE 1: BOTH ARE FREE */
	if(prev_free == 0 && next_free == 0) {
		unsigned int sizeof_prev = (*(target-1) >> 1)*4;
		unsigned int sizeof_next = (*(target + (available_size/4) + 1) >> 1)*4;
		unsigned int new_size = available_size + sizeof_next + sizeof_prev;
		header = target - (sizeof_prev/4);
		/*check to see if we are gobbling up root or tail*/
		int* next_add = target + (available_size/4);
		if (next_add == tail) 
			tail = (int*)((int64_t)*(tail+4));
		if (header==tail)
			tail = (int*)((int64_t)*(tail+4));
		if (next_add == tail) 
			tail = (int*)((int64_t)*(tail+4));
		if (header==tail)
			tail = (int*)((int64_t)*(tail+4));
		if (root == header)
			root = (int*)(int64_t)*(root+2);
		if (root == next_add)
			root = (int*)(int64_t)*(root+2); 
		if (root == header)
			root = (int*)(int64_t)*(root+2);
		if (root == next_add)
			root = (int*)(int64_t)*(root+2); 
		/*change prev of connecting block*/
		int* prevs_next = (int*)(int64_t)*(header+2);
		int* prevs_prev = (int*)(int64_t)*(header+4);
		int* nexts_next = (int*)(int64_t)*(next_add+2);
		int* nexts_prev = (int*)(int64_t)*(next_add+4);
		if (prevs_next == next_add)
			prevs_next = nexts_next;
		*(prevs_prev+2) = (int64_t)prevs_next;
		if (prevs_prev == next_add)
			prevs_prev = nexts_prev;
		*(prevs_next+4) = (int64_t)prevs_prev;
		if (nexts_next == header)
			nexts_next = prevs_next;
		*(nexts_prev+2) = (int64_t)nexts_next;
		if (nexts_prev == header)
			nexts_prev = prevs_prev;
		*(nexts_next+4) = (int64_t)nexts_prev;
		
		
		/*change header of block*/
		*header = (new_size - 16);
		*(header+1) = (new_size/16) << 3;
		/*change footer of block*/
		int* footer = target + (available_size/4) + (sizeof_next/4) - 2;
		*footer = new_size - 16;
		*(footer + 1) = (new_size/16) << 3;
		/*change prev of connecting block */
		*(root+4) = (int64_t)header;
		/*change next of connecting block */
		*(tail+2) = (int64_t)header;
		/*create next for block */
		*(header + 2) = (int64_t)root;
		/*create prev for block */
		*(header + 4) = (int64_t)tail;
		root = header;
	}
	/*CASE 2: prev is free, next isn't*/
	else if(prev_free == 0 && next_free == 1) {
		unsigned int sizeof_prev = (*(target-1) >> 1)*4;
		unsigned int new_size = available_size + sizeof_prev;
		header = target - (sizeof_prev/4);
		if (header == tail) {
			tail = (int*)((int64_t)*(tail+4));
		}
		int64_t nextblock_2 = *(header + 2);
		/*change prev of connecting block*/
		int* next_prev2 = (int*)nextblock_2;
		*(next_prev2+4) = 	(int64_t)*(header + 4);
		int64_t prevblock_2 = *(header + 4);
		/*change prev of connecting block*/
		next_prev2 = (int*)prevblock_2;
		*(next_prev2+2) = (int64_t)*(header + 2);
		/*change header of block*/
		*header = (new_size - 16);
		*(header+1) = (new_size/16) << 3;
		/*change footer of block*/
		int* footer = target + (available_size/4) - 2;
		*footer = new_size - 16;
		*(footer + 1) = (new_size/16) << 3;
		/*change prev's next*/
		int* prev = (int*)((int64_t)(header+4));
		*(prev+2) = (int64_t)header+2;
		/*change next's prev*/
		int* next = (int*)((int64_t)(header+2));
		*(next+4) = (int64_t)header+4;
		/*create next for block*/
		if (header!=root)
			*(header + 2) = (int64_t)root;
		/*create prev for block*/
		*(header + 4) = (int64_t)tail;
		root = header;
		*(tail+2) = (int64_t)root;
	}
	/*CASE 3: next is free, previous isn't*/
	else if(prev_free == 1 && next_free == 0) {
		unsigned int sizeof_next = (*(target + (available_size/4) + 1) >> 1)*4;
		int* next_add = target + (available_size/4);
		/*check tail conditions*/
		if (next_add == tail && next_add != root) 
			tail = (int*)((int64_t)*(tail+4));
		unsigned int new_size = available_size + sizeof_next;
		/*change header of block*/
		header = target;
		*header = (new_size - 16);
		*(header+1) = (new_size/16) << 3;
		/*change footer of block*/
		int* footer = target + (available_size/4) + (sizeof_next/4) - 2;
		*footer = new_size - 16;
		*(footer + 1) = (new_size/16) << 3;
		/*create next for block*/
		*(header + 2) = (int64_t)root;
		/*create prev for block*/
		*(header + 4) = (int64_t)tail;
		int64_t nextblock_2 = *(next_add + 2); 
		/*change prev of connecting block*/
		int* next_prev2 = (int*)nextblock_2;
		*(next_prev2+4) = 	(int64_t)*(next_add+4);
		int64_t prevblock_2 = *(next_add + 4);  
		/*change prev of connecting block*/
		next_prev2 = (int*)prevblock_2;
		*(next_prev2+2) = 	(int64_t)*(next_add + 2);
		if (nextblock_2==prevblock_2) {
			*(next_prev2+2) = 	(int64_t)*(root);
			*(next_prev2+4) = 	(int64_t)*(root);
			*(header + 2) = (int64_t)tail;
			*(header + 4) = (int64_t)tail;
		}
		if (next_add == tail && next_add == root){
			root = header;
			tail = header;
		}
		/*change prev of connecting block*/
		*(root+4) = (int64_t)header;
		/*change next of connecting block*/
		*(tail+2) = (int64_t)header;
		root = header;
	}
	/*CASE 4: neither are free. No coalescing*/
	else {
		/*change header*/
		header = target;
		*target = available_size - 16;
		*(target+1) = (available_size/16) << 3;
		/*change footer*/
		int* footer = target + ((available_size/4) - 2);
		*footer = available_size-16;
		*(footer+1) = (available_size/16) << 3;
		/*set prev*/
		int* new_prev = target+4;
		if (target==tail)
			tail = (int*)((int64_t)*(tail+4));
		*new_prev = (int64_t)tail;
		/*set next*/
		int* new_next = target+2;
		*new_next = (int64_t)root;
		/*change next's prev*/
		*(root+4) = (int64_t)target;
		/*change prev's next*/
		*(tail+2) = (int64_t)target;
		root = target;
	}}
	if (ADDRESS_c == 1) {
	/*CASE 1: BOTH ARE FREE */
	if(prev_free == 0 && next_free == 0) {
		unsigned int sizeof_prev = (*(target-1) >> 1)*4;
		unsigned int sizeof_next = (*(target + (available_size/4) + 1) >> 1)*4;
		unsigned int new_size = available_size + sizeof_next + sizeof_prev;
		/*change header of block*/
		header = target - (sizeof_prev/4);
		*header = (new_size - 16);
		*(header+1) = (new_size/16) << 3;
		/*change footer of block*/
		int* footer = target + (available_size/4) + (sizeof_next/4) - 2;
		*footer = new_size - 16;
		*(footer + 1) = (new_size/16) << 3;
		/*create next for block (no need to do for prev since it's already correct)*/
		int64_t next = *(target + ((available_size/4) + 2));
		*(header + 2) = next;
		/*change prev of connecting block (no need to do next, its already correct)*/
		int* nexts_prev = (int*) next;
		*(nexts_prev+4) = (int64_t)header;
		/*check root/tail*/
		int* next_block = target + (available_size/4); 
		if (next_block==tail)
			tail = header;
		if (next_block==root)
			root = header;
	}
	/*CASE 2: prev is free, next isn't*/
	else if(prev_free == 0 && next_free == 1) {
		unsigned int sizeof_prev = (*(target-1) >> 1)*4;
		unsigned int new_size = available_size + sizeof_prev;
		/*change header of block*/
		header = target - (sizeof_prev/4);
		*header = (new_size - 16);
		*(header+1) = (new_size/16) << 3;
		/*change footer of block*/
		int* footer = target + (available_size/4) - 2;
		*footer = new_size - 16;
		*(footer + 1) = (new_size/16) << 3;
	}
	/*CASE 3: next is free, previous isn't*/
	else if(prev_free == 1 && next_free == 0) {
		unsigned int sizeof_next = (*(target + (available_size/4) + 1) >> 1)*4;
		unsigned int new_size = available_size + sizeof_next;
		/*change header of block*/
		header = target;
		*header = (new_size - 16);
		*(header+1) = (new_size/16) << 3;
		/*change footer of block*/
		int* footer = target + (available_size/4) + (sizeof_next/4) - 2;
		*footer = new_size - 16;
		*(footer + 1) = (new_size/16) << 3;
		/*create next for block*/
		int64_t next = *(target + ((available_size/4) + 2));
		*(header + 2) = next;
		/*create prev for block*/
		int64_t prev = *(target + ((available_size/4) + 4));
		*(header + 4) = prev;
		/*change prev of connecting block*/
		int* nexts_prev = (int*) next;
		*(nexts_prev+4) = (int64_t)header;
		/*change next of connecting block*/
		int* prevs_next = (int*) prev;
		*(prevs_next+2) = (int64_t)header;
		/*check head/root*/
		int* next_block = target + (available_size/4); 
		if (next_block==tail)
			tail = target;
		if (next_block==root)
			root = target;
	}
	/*CASE 4: neither are free. No coalescing*/
	else {
		/*change header*/
		header = target;
		*target = available_size - 16;
		*(target+1) = (available_size/16) << 3;
		/*change footer*/
		int* footer = target + ((available_size/4) - 2);
		*footer = available_size-16;
		*(footer+1) = (available_size/16) << 3;
		if (((*(root+1)&1)==1) || ((*(tail+1)&1)==1)) {
			root = target;
			*(root+2) = (int64_t)root;
			*(root+4) = (int64_t)root;
			tail = target;
		}
		else {
		/*find prev*/
		int not_found = 1;
		int* cursor = root;
		int* prev = root;
		int* next = root;
		while (not_found){
			if (cursor < target){
				prev = cursor;
				if (prev == tail){
					not_found=0;
					tail = target;
				}
				cursor = (int*)((int64_t)*(prev+2));
			}
			else {
				not_found=0;
			}
		}
		if ((cursor==root) && (cursor>target)) {
			*(cursor+4) = (int64_t)target;
			*(target+4) = (int64_t)tail;
			*(target+2) = (int64_t)root;
			root = target;
		}
		if ((cursor==tail) && (cursor<target)) {
			*(cursor+2) = (int64_t)target;
			*(target+4) = (int64_t)cursor;
			*(target+2) = (int64_t)root;
			tail = target;
		}
		else {
			/*set prev*/
			int* new_prev = target+4;
			*new_prev = (int64_t)prev;
			/*set next*/
			int* new_next = target+2;
			*new_next = (int64_t)*(prev+2);
			/*change next's prev*/
			next = (int*)((int64_t)*(prev+2));
			*(next+4) = (int64_t)target;
			/*change prev's next*/
			*(prev+2) = (int64_t)target;
		}}
	}}
}

void* sf_realloc(void *ptr, size_t size) {
	int* target = (int*)ptr - 2;
	unsigned int prev_size = *(target);
	unsigned int available_size = (*(target+1) >> 1)*4;
	if ((size+available_size) >= 4294967296 || size==0){
		printf("ERROR: %s\n", strerror(ENOMEM));
		return NULL;
	}
	if (size <= available_size)
	{
		/*if we are here, we don't need to move the pointer*/
		*target = size;
		return ptr;
	}
	else {
		int* new_location = sf_malloc(size);
		target+=2;
		memcpy(new_location, target, prev_size);
		sf_free(ptr);
		return (void*)new_location;
	}
}


void sf_snapshot(void) {
	time_t rawtime;
	struct tm *tm;
	char str[100];
	time( &rawtime );
	tm = localtime( &rawtime );
	strftime(str,100,"%x - %I:%M%p", tm);
	printf("Explicit 8 %d\n\n# %s\n", heap_size, str);
	int* cursor = root;
	int heap_end = 0;
	unsigned int cursor_size = 0;
	if (heap_ptr == 0)
		return;
	while (heap_end!=1) {
		cursor_size = (*(cursor+1) >> 1)*4;
		printf("%p %d\n", cursor, cursor_size);
		if (cursor==tail)
			heap_end=1;
		heap_end++;
		cursor = (int*)((int64_t)*(cursor+2));
	}
}

void* sf_calloc(size_t nmemb, size_t size) {
	int amount = nmemb * size;
	if (amount >= 4294967296 || amount==0){
		printf("ERROR: %s\n", strerror(ENOMEM));
		return NULL;
	}
	char* location = sf_malloc(amount);
	int zero = 0;
	char* pointer= location;
	while (zero != amount) {
		*pointer = '\0';
		zero++;
		pointer = location+zero;
	}
	return (void*) location;
}