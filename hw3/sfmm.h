#include <stdlib.h>

#ifndef __SFMM_H
#define __SFMM_H

/**
* This routine will  \. It is called the
* `_start` function which is called before main is called.
*/
void sf_mem_init(void);
/**
* This is your implementation of malloc. It creates dynamic memory which
* is aligned and padded properly for the underlying system. This memory
* is uninitialized.
* @param size The number of bytes requested to be allocated.
* @return If successful, the pointer to a valid region of memory
* to use is returned, else the value NULL is returned and the
* ERRNO is set accordingly. If size is set to zero, then the
* value NULL is returned.
*/
void* sf_malloc(size_t size);
/**
* Marks a dynamically allocated region as no longer in use.
* @param ptr Address of memory returned by the function sf_malloc,
* sf_realloc, or sf_calloc.
*/
void sf_free(void *ptr);
/**
* Resizes the memory pointed to by ptr to be size bytes.
* @param ptr Address of the memory region to resize.
* @param size The minimum size to resize the memory to.
* @return If successful, the pointer to a valid region
* of memory to use is returned, else the value NULL is
* returned and the ERRNO is set accordingly.
*
* A realloc call with a size of zero should return NULL
* and set the ERRNO accordingly.
*/
void* sf_realloc(void *ptr, size_t size);
/**
* Allocate an array of nmemb elements each of size bytes.
* The memory returned is additionally zeroed out.
* @param nmemb Number of elements in the array.
* @param size The size of bytes of each element.
* @return If successful, returns the pointer to a valid
* region of memory to use, else the value NULL is returned
* and the ERRNO is set accordingly. If nmemb or
* size is set to zero, then the value NULL is returned.
*/
void* sf_calloc(size_t nmemb, size_t size);
/**
* Function which outputs the state of the free-list to stdout.
* See sf_snapshot section for details on output format.
*/
void sf_snapshot(void);

#endif
