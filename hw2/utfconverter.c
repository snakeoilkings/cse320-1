#include "utfconverter.h"

#define cse320(a,b,c,d,e,f,g,h) printf("DEBUG: \nCSE320: Host: %s\nCSE320: Input:%s, %d, %d, %d byte(s)\n CSE320: Output: %s \n CSE320: Input Encoding: %s\nCSE320: Output Encoding: %s\n", a, b, c,d,e,f,g,h);

int verbage = 0;    /*verbage is true for this amount of v's*/
int frmt = 0;       /*dictates format of output */
int inpt = -1;      /*determines BOM of file */
int little= true;	/*determines if the environment is little or big endian*/
int dc_flag = -1;	/* flag for debugger*/
char *input_path = NULL;
char *output_path = NULL;
char *format = NULL;
struct stat A;
struct stat B;

int main(int argc, char* argv[])
{
	unsigned int m = 1;
	char *c = (char*)&m; /* Convert the LSB into a character */
	if(*c) 
		little = false;
    int i, opt, return_code = EXIT_FAILURE;
	extern int optind;
	unsigned char read_value;
    /* Parse short options */
    while((opt = getopt(argc, argv, "hved")) != -1) {
        switch(opt) {
            case 'h':
                /* The help menu was selected */
                USAGE(argv[0]);
                exit(EXIT_SUCCESS);
                break;
			
			case 'v':
				/* FIGURES OUT WHAT IM PRINTING TO STDOUT */
				verbage++;
				break;
			case 'e':
				/* What format */
				frmt=1;
				break;
			case 'd':
				dc_flag=1;
				break;
            case '?':
                /* Let this case fall down to default;
                 * handled during bad option.
                 */
            default:
                /* A bad option was provided. */
                USAGE(argv[0]);
                exit(EXIT_FAILURE);
                break;
        }
    }
    /* Get position arguments */
    if(optind < argc && (argc - optind) == 3) {
		format = argv[optind++];
        input_path = argv[optind++];
        output_path = argv[optind++];
    } else {
        if((argc - optind) <= 0) {
            fprintf(stderr, "Missing INPUT_FILE and OUTPUT_FILE.\n");
        } else if((argc - optind) == 1) {
            fprintf(stderr, "Missing OUTPUT_FILE.\n");
        } else {
            fprintf(stderr, "Too many arguments provided.\n");
        }
        USAGE(argv[0]);
        exit(EXIT_FAILURE);
    }
	/* gets type for format, if there is none, quits */
	frmt = 0;
	for (i=0; i < argc; i++) {
		if (!strcmp(format, "UTF-8"))
			frmt = 1;
		if (!strcmp(format, "UTF-16LE"))
			frmt = 2;
		if (!strcmp(format, "UTF-16BE"))
			frmt = 3;
		if (frmt == 0) {
			printf("No BOM on input file. Exiting...");
			USAGE(argv[0]);
			return EXIT_FAILURE;
		}
    }

    /* Make sure all the arguments were provided */
    if(input_path != NULL && output_path != NULL) {
        int input_fd = -1, output_fd = -1;
        bool success = false;
        switch(validate_args(input_path, output_path)) {
                case VALID_ARGS:
                    /* Attempt to open the input file */
					input_fd = open(input_path, O_RDONLY);
                    if(input_fd < 0) {
                        fprintf(stderr, "Failed to open the file %s\n", output_path);
                        perror(NULL);
                        goto conversion_done;
                    }
					/*get BOM, exit failure if none */
					read(input_fd, &read_value, 1);
					if (read_value== 0xEF) {
						inpt = 0;
						lseek(input_fd, 1, SEEK_CUR);
					}
					else if (read_value==0xFF){
						inpt = 1;
					}
					else if (read_value==0xFE) {
						inpt = 2;
					}
					else{
						/*No BOM, exit failure */
						printf("Input file missing byte order mark. Exiting...");
						exit(EXIT_FAILURE);	
					}
					lseek(input_fd, 1, SEEK_CUR);
                    /* Delete the output file if it exists; Don't care about return code. */
                    unlink(output_path);
                    /* Attempt to create the file */
                    if((output_fd = open(output_path, O_CREAT | O_WRONLY,
                        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)) < 0) {
                        /* Tell the user that the file failed to be created */
                        fprintf(stderr, "Failed to open the file %s\n", input_path);
                        perror(NULL);
                        goto conversion_done;
                    }
                    /* Start the conversion */
                    success = convert(input_fd, output_fd);
conversion_done:
                    if(success) {
                        /* We got here so it must of worked right? */
                        return_code = EXIT_SUCCESS;
						printf("\nThe file %s was succesfully created.\n", output_path);
                    } else {
                        /* Conversion failed; clean up */
                        if(output_fd < 0 && input_fd >= 0) {
                            close(input_fd);
                        }
                        if(output_fd >= 0) {
                            unlink(output_path);
                        }
                        /* Just being pedantic... */
                        return_code = EXIT_FAILURE;
                    }
					break;
                case SAME_FILE:
                    fprintf(stderr, "The output file %s was not created. Same as input file.\n", output_path);
                    break;
                case FILE_DNE:
                    fprintf(stderr, "The input file %s does not exist.\n", input_path);
                    break;
                default:
                    fprintf(stderr, "An unknown error occurred\n");
                    break;
        }
    } else {
        /* Alert the user what was not set before quitting. */
        if(input_path == NULL) {
            fprintf(stderr, "INPUT_FILE was not set.\n");
        }
        if(output_path == NULL) {
            fprintf(stderr, "OUTPUT_FILE was not set.\n");
        }
        /* Print out the program usage */
        USAGE(argv[0]);
    }
	return return_code;
}

int validate_args(const char* input_path, const char* output_path)
{
    int return_code = FAILED;
    /* Make sure both strings are not NULL */
    if(input_path != NULL && output_path != NULL) {
        /* Check to see if the the input and output are two different files. */
		/*make stats for each, sheck their info to prevent linking issues*/
		struct stat *sa=&A;
		struct stat *sb=&B;
		stat(output_path, sa);
		stat(input_path, sb);
        if(A.st_dev !=B.st_dev || A.st_ino != B.st_ino) {
            if(stat(input_path, sb) == -1) {
                /* something went wrong */
                if(errno == ENOENT) {
                    /* File does not exist. */
                    return_code = FILE_DNE;
                } else {
                    /* No idea what the error is. */
                    perror(NULL);
                }
            } else {
                return_code = VALID_ARGS;
            }
        }
    }
    return return_code;
}

bool convert(const int input_fd, const int output_fd)
{
    bool success = false;
    /*bool parse_error = false;*/
    if(input_fd >= 0 && output_fd >= 0) {		
		/*add BOM*/
		unsigned char d1= 0xFE;
		unsigned char d2= 0xFF;
		unsigned int UTF8 = 0xBFBBEF;
		unsigned char e1 = 0xBf;
		unsigned char e2 = 0xBB;
		unsigned char e3 = 0xEF;
		if (frmt ==1){
			if (little) {
				write(output_fd, &e3, 1);
				write(output_fd, &e2, 1);
				write(output_fd, &e1, 1);
			}
			else
				write(output_fd, &UTF8, 3);
		}
		if (frmt==2){
			write(output_fd, &d2, 1);
			write(output_fd, &d1, 1);
		}
		if(frmt==3) {
			write(output_fd, &d1, 1);
			write(output_fd, &d2, 1);
		}
        /* UTF-8 encoded text can be @ most 4-bytes */
        unsigned char bytes[4];
        unsigned char read_value;
        size_t count = 0;
        ssize_t bytes_read = 1;
        bool encode = false;
        int w1, w2;
		char* verb1 = "-------------+-------------+";
		char* verb2 = "    input    |    output   |";
        /*print neat output */
		switch(verbage) {
			case 1:
				verb1="";
				verb2="";
				break;
			case 2:
				verb1 = "-------------+";
				verb2 = "    input    |";
				break;
			default:
				break;
		}
        if (verbage > 0) {
            printf("\n+---------+------------+-----------+%s", verb1);
            printf("\n|  ASCII  | # of bytes | codepoint |%s", verb2);
			printf("\n+---------+------------+-----------+%s", verb1);
		}
        /* Read in UTF-8 Bytes */
        while(bytes_read == 1) {
			/*if 8 bit file, only read in 1 bytes*/
			bytes_read = read(input_fd, &read_value, 1);
			if (inpt !=0) {
				if (read_value==0) {
					bytes_read = read(input_fd, &read_value, 1);
				}
			}
			if (bytes_read!=1 && count > 0){
				encode=true;
			}
			if (bytes_read!=1 && count <1)
				break;
			else {
            /* Mask the most significate bit of the byte */
            unsigned char masked_value = read_value & 0x80;
            if(masked_value == 0x80) {
                if((read_value & UTF8_4_BYTE) == UTF8_4_BYTE ||
                   (read_value & UTF8_3_BYTE) == UTF8_3_BYTE ||
                   (read_value & UTF8_2_BYTE) == UTF8_2_BYTE) {
                    /* Check to see which byte we have encountered */
                    if(count == 0) {
                        bytes[count] = read_value;
						count++;
                    } else {
                        /* Set the file position back 1 byte */
                        if(lseek(input_fd, -1, SEEK_CUR) < 0) {
                            /* failed to move the file pointer back */
                            /*parse_error = true;*/
                            perror(NULL);
                            goto conversion_done;
                        }
                        /* Encode the current values into UTF-16LE */
                        encode = true;
                    }
                } else if((read_value & UTF8_CONT) == UTF8_CONT) {
                    /* continuation byte */
                    bytes[count] = read_value;
                    count++;
                }
            } else {
                if(count == 0) {
                    /* US-ASCII */
                    bytes[count] = read_value;
                    count++;
                    encode = true;
                } else {
                    /* Found an ASCII character but theres other characters
                     * in the buffer already.
                     * Set the file position back 1 byte.
                     */
                    if(lseek(input_fd, -1, SEEK_CUR) < 0) {
                        /* failed to move the file pointer back */
                        /*parse_error = true;*/
                        perror(NULL);
                        goto conversion_done;
                    }
                    /* Enocde the current values into UTF-16LE */
                    encode = true;
                }
            }
			}
			
            /* If its time to encode do it here */
            if(encode) {
                unsigned int i;
				unsigned int value = 0;
                bool isAscii = false;
                for(i=0; i < (int)count; i++) {
                    if(i == 0) {
                        if((bytes[i] & UTF8_4_BYTE) == UTF8_4_BYTE) {
                            value = bytes[i] & 0x7;
                        } else if((bytes[i] & UTF8_3_BYTE) == UTF8_3_BYTE) {
                            value =  bytes[i] & 0xF;
                        } else if((bytes[i] & UTF8_2_BYTE) == UTF8_2_BYTE) {
                            value =  bytes[i] & 0x1F;
                        } else if((bytes[i] & 0x80) == 0) {
                            /* Value is an ASCII character */
                            value = bytes[i];
                            isAscii = true;
                        } else {
                            /* Marker byte is incorrect */
                            /*parse_error = true;*/
                            goto conversion_done;
                        }
                    } else {
                        if(!isAscii) {
							value = (value << 6) | (bytes[i] & 0x3F);
                        } else {
                            /* How is there more bytes if we have an ascii char? */
                            /*parse_error = true;*/
                            goto conversion_done;
                        }
                    }
                }
                /* Handle the value if its a surrogate pair*/
                if(value >= SURROGATE_PAIR) {
                    int vprime; /* v` = v - 0x10000 */
					vprime = value - SURROGATE_PAIR; 
					/* subtract the constant from value */
					w1 = (vprime >> 10) + 0xD800;
					w2 =  (vprime & 0x3FF) + 0xDC00;
                    /* write the surrogate pair w1 to file */
					if (frmt==3) {
                    if(!safe_write(input_fd, output_fd, &w1, 2)) {
                        /*parse_error = true;*/
                        goto conversion_done;
                    }
                    /* write the surrogate pair w2 to file */
                    if(!safe_write(input_fd, output_fd, &w2, 2)) {
                        /*parse_error = true;*/
                        goto conversion_done;
                    }
					}
					if (frmt==2) {
						 if(!safe_write(input_fd, output_fd, &w1+1, 1)) {
                        /*parse_error = true;*/
                        goto conversion_done;
                    }
                    /* write the surrogate pair w2 to file */
                    if(!safe_write(input_fd, output_fd, &w1, 1)) {
                        /*parse_error = true;*/
                        goto conversion_done;
                    }
					 if(!safe_write(input_fd, output_fd, &w2+1, 1)) {
                        /*parse_error = true;*/
                        goto conversion_done;
                    }
                    /* write the surrogate pair w2 to file */
                    if(!safe_write(input_fd, output_fd, &w2, 1)) {
                        /*parse_error = true;*/
                        goto conversion_done;
                    }
					}
                    if (verbage > 0) {
                        printf("\n|   NONE  |     %d      |  U+%06x |",(int)count, value);
						if (verbage > 1){
							printf("   0x%x", value);
							for (i=0; i <(4-count)*2; i++)
								printf(" ");
							printf("|");
						}
						if (verbage > 2){
							if (frmt == 1){
								if (count==4)
									printf("   0x%x   |", value);
								if (count==3)
									printf("   0x%x     |", value);
							}
							if (frmt == 3)
								printf("  0x%x%x |", w1, w2);
							if (frmt == 2){
								/* PUT CODE HERE */
							}
							
						}
					}
                } else {
					unsigned int value2=0;
                    /* write the codeunit to file */
					if (little) {
						unsigned char val = *(&value+3);
						unsigned char val2 = *(&value+2);
						if (value < 256) {
							if (inpt==0){
								if (frmt==3)
								if(!safe_write(input_fd, output_fd, &value2, 1)) {
								/*parse_error = true;*/
									goto conversion_done;
								}
								if(!safe_write(input_fd, output_fd, &val, 1)) {
									/*parse_error = true;*/
									goto conversion_done;
								}
								if (frmt==2)
								if(!safe_write(input_fd, output_fd, &value2, 1)) {
								/*parse_error = true;*/
									goto conversion_done;
								}
							}
							if (inpt!=0 && frmt==1) {

									if(!safe_write(input_fd, output_fd, &val, 1)) {
									/*parse_error = true;*/
										goto conversion_done;
									}

							}
							if (inpt!=0 && frmt!=1) {
								if (frmt==3) {
									if(!safe_write(input_fd, output_fd, &value2, 1)) {
										/*parse_error = true;*/
										goto conversion_done;
									}
								}
								unsigned char val = *(&value+3);
								if(!safe_write(input_fd, output_fd, &val, 1)) {
									/*parse_error = true;*/
									goto conversion_done;
								}
								if (frmt==2) {
									if(!safe_write(input_fd, output_fd, &value2, 1)) {
										/*parse_error = true;*/
										goto conversion_done;
									}
								}
							}
						}
						if (value > 255) {
							int i;
							printf("%d\n", value);
							for (i = 0; i < 8; i++) {
							printf("%d", !!((val << i) & 0x80));
							}
							printf ("\n");
							for (i = 0; i < 8; i++) {
							printf("%d", !!((val2 << i) & 0x80));
							}
							printf ("\n");
							safe_write(input_fd, output_fd, &val, 1);
							safe_write(input_fd, output_fd, &val2, 1);
						}
						
					}
					if(!little) {
						if (inpt==0 && value < 256) {
							if(!safe_write(input_fd, output_fd, &value2, 1)) {
								/*parse_error = true;*/
								goto conversion_done;
							}
							if(!safe_write(input_fd, output_fd, &value, 1)) {
								/*parse_error = true;*/
								goto conversion_done;
							}
						}
						if (inpt==0 && value > 255) {
							if(!safe_write(input_fd, output_fd, &value, 1)) {
								/*parse_error = true;*/
								goto conversion_done;
							}	
							if(!safe_write(input_fd, output_fd, &value+1, 1)) {
								/*parse_error = true;*/
								goto conversion_done;
							}
						}
						/* UTF16 to UTF8 */
						if (inpt!=0 && frmt==1) {
								if(!safe_write(input_fd, output_fd, &value, 1)) {
									/*parse_error = true;*/
									goto conversion_done;
								}
						}
						if (inpt!=0 && frmt!=1) {
							if (frmt==3) {
							if(!safe_write(input_fd, output_fd, &value2, 1)) {
								/*parse_error = true;*/
								goto conversion_done;
							}
							}
							if(!safe_write(input_fd, output_fd, &value, 1)) {
								/*parse_error = true;*/
								goto conversion_done;
							}
							if (frmt==2) {
							if(!safe_write(input_fd, output_fd, &value2, 1)) {
								/*parse_error = true;*/
								goto conversion_done;
								}
							}
						}
					}
					if (value < 256 && value > 31 && verbage > 0) {
						printf("\n|    %c    |     %d      |  U+%04x   |", value, (int)count, value);
						if (verbage > 1)
							printf("   0x%-4x    |", value);
					}
					if (value > 255 && verbage > 0) {
						printf("\n|   NONE  |     %d      |  U+%04x   |", (int)count, value);
						if (verbage > 1){
							printf("   0x%x", value);
							for (i=0; i <(5-count)*2; i++)
								printf(" ");
							printf("|");
						}
					}
					if (verbage > 2 && value < SURROGATE_PAIR && value > 31){
							if (frmt == 1) {
								if (value < 256 && value > 31)
									printf("   0x%x      |", value);
								if (value > 255) {
									printf("   0x%x", value);
									for (i=0; i <(5-count)*2; i++)
										printf(" ");
									printf("|");
								}
							}
							else if (frmt == 3)
								printf("  0x%04x     |", value);
							else {
								if (value < 256)
									printf("  0x%x00     |", value);
								else
									printf("  0x%x     |", value);
							}
					}			
                }
                /* Done encoding the value to UTF-16LE */
                encode = false;
                count = 0;
            }
        }
        /* If we got here the operation was a success! */
        success = true;
		if (verbage > 0) 
            printf("\n+---------+------------+-----------+%s", verb1);
    }
conversion_done:
			;
			
			char name[30];
			memset(name, '\0', 31);
			gethostname(name, 29);
			#ifdef CSE320
			char* bom_in;
			if (inpt==0)
				bom_in="UTF-8";
			if (inpt==1)
				bom_in="UTF-16LE";
			if (inpt==2) 
				bom_in="UTF-16BE";
			cse320(name, input_path, (int)A.st_dev , (int)A.st_ino , (int)A.st_size, output_path, bom_in, format);
			#endif
	    return success;
}

bool safe_write(const int input_fd, const int output_fd, void* value, size_t size)
{
    bool success = true;
    ssize_t bytes_written;
	bytes_written = write(output_fd, value, size);
    if((size_t)bytes_written != size) {
        /* The write operation failed */
        fprintf(stderr, "Write to file failed. Expected %d bytes but got %d \n%d\n", (int)size, (int)bytes_written, input_fd);
        success = false;
    }
    return success;
}