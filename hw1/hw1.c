#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "converter.h"

int main(int argc, char *argv[]) {
	char *endian = argv[1];
	char *output = argv[2];
	char bytes[17];
	int i;
	int retValue = 0;
	int endLength = strlen(endian);
	int bigOrLittle = 0;   // -1 if neither, 1 if big, 0 if little
	int outputType = -1;   // -1 if none, 1 if 1's comp, 1 if SM, and 2 if E-1023
	int msb = -1;
	
	//clears mem for the number in chars
	memset(bytes, '\0', 17);
	
	//checks to see if most significant bit is > 7 (making the number negative)
	if (argc == 11 && strlen(argv[10])==2 && argv[10][0] > 55)
		msb = (int)argv[argc-1][0];
	
	//checks to see if there are too many or too few inputs
	if (argc > 11 || argc < 4)
		return EXIT_FAILURE;
	
	//checks validity of endian input
	if (endLength != 2)
		return EXIT_FAILURE;
	bigOrLittle = (endian[1] == 'e') + (endian[1] == 'E');
	if (!bigOrLittle)
		return EXIT_FAILURE;
	bigOrLittle = -1;
	if ((endian[0] == 'B') + (endian[0] == 'b'))
		bigOrLittle = 1;
	if ((endian[0] == 'L') + (endian[0] == 'l'))
		bigOrLittle = 0;
	if (bigOrLittle == -1)
		return EXIT_FAILURE;
	
	//checks validity of output input
	if (output[0] == '1')
		outputType = 0;
	if ((output[0] == 'e') + (output[0] == 'E'))
		outputType = 2;
	retValue = (output[0]=='s') + (output[0]=='S') + (output[1]=='m') + (output[1]=='M');
	if (retValue == 2)
		outputType = 1;
	if (outputType == -1)
		return EXIT_FAILURE;

	// makes 1 string out of all the arguments of LE
	for (i=(argc-1); i > 2; i--)
	{		
		//invalid entry if any parameter is more than 2 digits
		if (strlen(argv[i]) > 2)
		{
			return EXIT_FAILURE;
		}
		if (strlen(argv[i])==1)
		{
			strcat(bytes, "0");
		}
		strcat(bytes, argv[i]);
	}
		
	//calls function to convert, and exits if -1 is returned
	unsigned long long hexprint = hex_convert(bytes);
	if (hexprint == -1) {
		return EXIT_FAILURE;
	}
	
	//takes care of SM and 1's compliment if they are negative numbers: -1 from them
	if (outputType!=2 && msb > 55)
		hexprint = hexprint-1;
	
	//takes care of Excess-1023 by -1023
	if (outputType==2) {
		hexprint = hexprint - (long long)1023;
	}
	
	//case: LE of type excess-1023 OR type 1's compliment (no bit conversion)
	if (bigOrLittle == 0 && outputType != 1) {
		printf("%016llx\n", hexprint);
	}

	//case: LE and signed magnitude. prints like before if positive
	if (bigOrLittle == 0 && outputType == 1) {
		if (msb > 55) {
			unsigned long long j = hexprint^9223372036854775807;
			printf("%016llx\n", j);
		}
		else {	
			printf("%016llx\n", hexprint);
		}
	}
	
	//case: BE of all 3 types as long as it is positive
	if ((bigOrLittle == 1 && outputType != 1) || (bigOrLittle==1 && outputType==1 && msb<56)) {
		memset(bytes, '\0', 17);
		sprintf(bytes, "%016llx", hexprint);
		printf("%.*s%.*s%.*s%.*s", 2, bytes+14, 2, bytes+12, 2, bytes+10, 2, bytes+8);
		printf("%.*s%.*s%.*s%.*s\n", 2, bytes+6, 2, bytes+4, 2, bytes+2, 2, bytes);
	}
	//case: BE of a negative to signed magnitude (bit conversion needed)
	if (bigOrLittle == 1 && msb > 55 && outputType==1) {
		unsigned long long j = hexprint^9223372036854775807;
		sprintf(bytes, "%016llx", j);
		printf("%.*s%.*s%.*s%.*s", 2, bytes+14, 2, bytes+12, 2, bytes+10, 2, bytes+8);
		printf("%.*s%.*s%.*s%.*s\n", 2, bytes+6, 2, bytes+4, 2, bytes+2, 2, bytes);
		
	}
	return EXIT_SUCCESS;
}