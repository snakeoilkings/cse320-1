#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "converter.h" 

int is_valid (const char* value)
{	
	int length = strlen(value);
	int val = 1;
	while (length > 0) 
	{
		length--;
		if (!(isdigit(value[length])) && (((int)value[length] < 65 || (int)value[length] > 102)))
		{
			val = 0;
		}
		if (!(isdigit(value[length])) && (int)value[length] > 70 && (int)value[length] < 97)
		{
			val = 0;
		}
	}
	if (val)
	{
		return 0;
	}
	return -1;
}

unsigned long long hex_convert(const char* input) {
	int isValid = is_valid(input);
	if (isValid == -1)
	{
		return -1;
	}
	char *ptr;
	unsigned long long hex = strtoull(input, &ptr, 16);
	return hex;
}
