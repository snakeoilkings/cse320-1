#ifndef CONVERTER_H_
#define CONVERTER_H_

int is_valid (const char* value);

unsigned long long hex_convert(const char* input);

#endif