#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

/* Assume no input line will be longer than 1024 bytes */
#define MAX_INPUT 1024

int 
main (int argc, char ** argv, char **envp) {

  int finished = 0;
  char *prompt = "320sh> ";
  char cmd[MAX_INPUT];


  while (!finished) {
    char *cursor;
    char last_char;
    int rv;
    int count;


    /* Print the prompt */
    rv = write(1, prompt, strlen(prompt));
    if (!rv) { 
      finished = 1;
      break;
    }
    
    /* read and parse the input */
    for(rv = 1, count = 0, 
	  cursor = cmd, last_char = 1;
	rv 
	  && (++count < (MAX_INPUT-1))
	  && (last_char != '\n');
	cursor++) { 

      rv = read(0, cursor, 1);
      last_char = *cursor;
    } 
    *cursor = '\0';

    if (!rv) { 
      finished = 1;
      break;
    }

    /* Execute the command, handling built-in commands separately */

	/* tokenize the string to get first command */
	const char delim[2] = " ";
	char* input = strtok(cmd, delim);
	
	/* turn into an editable string */
	int input_length = strlen(input);
	char input_str[input_length];
	strcpy(input_str, input);
	
	/* change string to lower case, and remove trailing \n if it exists */
	int i;
	for (i=0; i < input_length; i++){
		if (input_str[i]=='\n') {
			input_str[i]='\0';
			break;
		}
		input_str[i] = tolower(input_str[i]);
	}
	
	/* if the new string = exit, then exit */
	int is_exit = strcmp(input_str, "exit");
	if (!is_exit)
		return EXIT_SUCCESS;
	
	//write(1, cmd, strnlen(cmd, MAX_INPUT));
  }

  return 0;
}
